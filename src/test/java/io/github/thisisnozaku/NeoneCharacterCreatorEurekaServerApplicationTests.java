package io.github.thisisnozaku;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = NeoneCharacterCreatorEurekaServerApplication.class)
public class NeoneCharacterCreatorEurekaServerApplicationTests {

	@Test
	public void contextLoads() {
	}

}
