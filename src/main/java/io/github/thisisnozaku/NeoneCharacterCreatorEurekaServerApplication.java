package io.github.thisisnozaku;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class NeoneCharacterCreatorEurekaServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeoneCharacterCreatorEurekaServerApplication.class, args);
	}
}
